# Pizza Order
pizza=input("Hello.. What size pizza do you want? Small, Medium, Large\n")
pizza_size=pizza.upper() # in case the user enter P as p, since python is case sensitive, it will throw an error. to avoid, i used upper() and convert the letters to uppercase. whatever user types, it will be converted to uppercase.
price=0
if pizza_size == "SMALL":
    price += 15 # we did += since we already initialized price to zero
elif pizza_size == "MEDIUM":
    price += 18
else:# pizza_size == "LARGE":
    price += 21
        
toppings=input("Do you want to add pepperoni? Y or N:  ")
if toppings == 'Y': # Don't forget to put Y within quotes since it is a string. I missed and i got error
    if pizza_size == "SMALL":
        price += 2
    else:
        price += 3

extra_cheese=input("Do you want to add extra cheese? Y or N:  ")
if extra_cheese == 'Y':
    price += 1

print(f'Final price is ${price}')
