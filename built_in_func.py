# abs() - returns absolute calue of a number
a=abs(-3.879)
print(a)

# following 2 built in functions return the binary version of an integer bin() and format()
a=bin(256) # returns the binary version of 256
print(a)

x=format(256,'b') # formats 256 to binary format. basically returns the binary value
print(x) # using format,we can format different ntegers to octal-'o',x-hexadcimal,'g'-general format,etc.

x=format(38, 'g') # prints the general format of an this integer
print(x)

# enumerate()- enumerate(iterable, start)
# enumerate() function takes a collection(lists, tuples,dictionary) and returns it as an enumerate object
# enumerate() adds a counter(number) as the key to the enumerate object(gives indexes to the items we give)

a=("apple", "Kiwi","papaya")
b=enumerate(a,2)# a-iterable object;by giving 2, you tell the start number of the enumerate object to start from. default is zero. You can give whatevernumber you eant the list to stat from
#Important: print(b) -if you just give b in print, it gives the enumerate object id. it puts the result as a list. so, give list(b) in print.
print(list(b))

# Using min()  and max()-The min() function returns the item with the lowest value, or the item with the lowest value in an iterable.
#If the values are strings, an alphabetically comparison is done.
x = min("Mike", "John", "Vicky")
print(x) # returns John, since strings are compared alphabetically 

x=max("geetha", "Janya","yuthi") # returns alphabetically higher iterable
print(x)

x=min(3,23,98) # max() returns the largest item in an iterable
print(x)

x=max(3,23,98)
print(x)

# Using pow()-returns exponential value
x=pow(2,5)# returns the value of 2 to the power 5
print(x)

#range()- range(9), range(2,10), range (1,11,2) types

x = range(6) # creates a sequence of numbers and prints each item in the sequence
for n in x: # range always starts at zero excludes the last number.
  print(n) # prints from 0 to 5, one by one

#Ex-2:  
x = range(3, 6) # creates sequence of numbers form 3 to 5 excluding 6 and INCREMENTS by 1
for n in x:
  print(n)
  
#Ex:3
x = range(3, 20, 2) # creates sequence from 3 to 19 and INCREMENTS by 2 instead of 1
for n in x:
  print(n)

