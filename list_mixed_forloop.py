#string and numeric data types are often used in groups called collections.
#Three such collections that Python supports are the list, the tuple, and the dictionary.

mylist = ["flight", "bus","train"] # list position starts at zero.
print(mylist)
print(type(mylist))
print(mylist[0])
print(mylist[1])
print(mylist[2])

# changing values in a list

mylist[0] = "ship" 
print(mylist)

# Mixed data type list

#You can mix or add several data types in a list. Other languages doesn't have this capability

mymixedlist = [4, 6.87, False, 45972, "I love my family", "1890"]
print(mymixedlist) # will just print the items in the list
for item in mymixedlist:
    print("{} is the data type of {} ".format(item, type(item))) #prints the data type of each item in the list using format function.
    
    # you can use for loops in 3 different ways. 
    # for item in mylist: Using 'in' keyword and giving a list
    # for(i=o, i>3, i++): Using incremental of i. that is i as an iteration variable
    # for (i=)    by giving range. check google