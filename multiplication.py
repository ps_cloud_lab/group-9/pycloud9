for i in range(12):
    print(f'Multiplication Table {i+1}')
    print("***************************")
    for j in range(13):
        mult=(i+1)*(j+1) # incremented i and j value to one, since the range starts at zero.we dont need zero table
        print(f'Multiplication of {(i+1)}*{(j+1)}={mult}')
    print("***************************")