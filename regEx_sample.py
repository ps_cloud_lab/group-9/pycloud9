import re
txt = ("GoodGood Good Morning 123")
r=re.findall("^G.*ing$",txt)
print(r)
if r:
        print("match found")
else:
        print("No match")

t=re.search("[o]",txt)
print(t.start())

s=re.sub("Good","Hello",txt,2) # repalces good 2 times and 3rd good doesn't get replaced
print(s)

s=re.split("ll", re.sub("Good","Hello",txt,2),1) # repalces good 2 times and 3rd good doesn't get replaced
print('nested re : ',s)

t=re.split("oo",txt)
print(t)

txt1='''ORIGIN      
        1 malwmrllpl lallalwgpd paaafvnqhl cgshlvealy lvcgergffy tpktrreaed
       61 lqvgqvelgg gpgagslqpl alegslqkrg iveqcctsic slyqlenycn
//'''
y=re.sub("ORIGIN","",txt1)
#print(y)
y=re.sub("\d","",y)
#print(y)
y=re.sub("\s","",y)
#print(y)
y=re.sub("//","",y)
print(y)
print(len(y))

t=(y[:24])
print(f' Amino acids 1-24: {t}')
print(len(t))

u=y[24:54]
print(f' Amino acids 25-54: {u}')
print(len(u))

v=y[54:89]
print(f' Amino acids 55-89: {v}')
print(len(v))

z=y[89:]
print(f' Amino acids 90-110: {z}, {len(z)}')

f = open("y.txt")
print(f.read())





        







