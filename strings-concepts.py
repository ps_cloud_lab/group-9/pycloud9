# using powerful f-string : Entering diff data types in a string using f string
#Instead of converting different variables to thie respective data types, you use f string to convert every variable into string.
#Ex: 
string="ram"
number=23
isString=True
print(f"Your name is {string}, and your value is {number}, and you are {isString}")#we cut down time on entering diff data types in the string


#Write a program that adds the digits in a 2 digit number. e.g. if the input was 35, then the output should be 3 + 5 = 8
two_digit_number = input("Enter two digit number\n")

# check the type of two digit number

print(type(two_digit_number))

# adds the digits what you entered. giving indexes to a string is called subscripting. strings are subscriptable.
new_number=two_digit_number[0]+two_digit_number[1] # gives error since two digit number is a string
 
 # convert string type to int type in order to add both the numbers
 
new_number=int(two_digit_number[0])+int(two_digit_number[1])
print(new_number)

# calculate bmi of a user
weight=int(input("Enter your weight ")) # converting string o/p from input function to int by type casting int.
height=int(input("Enter your height ")) # instead of converting here as int, we can type cast in weight and height.bmi=int(weight)/float(height)**2.o/p will be in float whcih we can convert into int
bmi=weight/height**2
print(int(bmi)) # I want bmi in int instead of float
