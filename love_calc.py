# LOVE Calculator
# for this, you need to check the comparing names with two special words LOVE and TRUE. this is how usually done.
# how many letters from true(t,r,u,e) is in your names. do the same with love. do your names has letters from love.
# count the number of times each letter and add them to get the percentage.

name1=input("Enter your name ")
name2=input("Enter the name you want to compare with  ")
name=name1 + name2
name_lowercase=name.lower()

t=name_lowercase.count('t')
r=name_lowercase.count('r')
u=name_lowercase.count('u')
e=name_lowercase.count('e')
sum_true = t + r + u + e
print(sum_true)#. just check. we dont nedd this

l=name_lowercase.count('l')
o=name_lowercase.count('o')
v=name_lowercase.count('v')
e=name_lowercase.count('e')
sum_love= l + o + v + e
print(sum_love)
print(type(sum_love)) # sum_love is int type. if we concatenate sum_love and sum_true, o/p will be 4+2=6. but we need 4 and 2
                        #next to each other as 42. only string concatenation will place both numbers side by side. so convert sumlove&sumtrue to string type
love_score = int(str(sum_true) + str(sum_love))
print(type(love_score)) #now, lovescore is str type. But when we need to do comparison below, we cant make string comparison. it needs to be int type.
                        #so, type cast string type to int for lovescore since lovescore variable will be compared below.so, go ahead and type cast int to love
                        #in the above line for lovescore. or you can create a new variable and type cast int to love score as below in the next line.
#love_score_int=int(love_score). 
print(love_score)
if (love_score < 10) or (love_score > 90):
    print(f'Your score is {love_score}, you go together like coke and mentos.')
if (love_score >= 40) and (love_score <= 50):
    print(f'"Your score is {love_score}, you are alright together.')
else:
    print(f"Your score is {love_score}.")


    
    
