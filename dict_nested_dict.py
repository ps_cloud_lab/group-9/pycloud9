#A dictionary is a list with named positions (keys). Means, the names in the list is given a name.
#Here, porsche,tesla and honda are lists and they are identified by names(arun, janya, yuthi) 
#instead of index numbers as in list and tuples.
# # ways to create dict. 1- by using dict() function. 2- initiazing dict name and creating the dict skeleton like example 1. 3rd way istheu example 2.


# Example-1  Defining a dictionary. Arun is key and Porsche is a value

mycardictionary = {
    "Arun" : "Porsche",
    "Janya": "Tesla",
    "Yuthi": "Honda"
}
print(mycardictionary)
print(type(mycardictionary)) # will display results within curly braces

# Accessing a dictionary by name

print(mycardictionary["Janya"]) # to access Janya's favorite car
print(mycardictionary["Yuthi"]) # to access Yuthi's fav car

# Another way to create dictionary is by using dict() bult-in function.
# Another Way of creating dictionary is using dict() built-in function

x = dict(name = "John", age = 36, country = "Norway")
print(x) # check the result. created the dictionary

# Nested dictionary and how to access the value from the nested dictionary

dict1={
    "Arun":"Cool Dad",
    "Janya":"Smart",
    "Yuthi":"Chutti",
    "Geetha": { "Amma":"MySoul","Dad":"My Hero"} # Imp:We need to give key to nested dictioanry.(named key here is Geetha to the nested dictioanry.Without key,you wont be able to access the value.
    }
print(dict1.get("Arun"))  # gives the o/p as cool dad. same as print(dict1["Arun"]).difference is if you use .get(), 
print(dict1.get("Geetha").get("Dad")) # same value as the below command.with get(),you give () and without get(), you give []
print(dict1["Geetha"]["Amma"]) # it says get the value of named key Geetha,inside Geetha, get the value of Amma. o/p will be Mysoul

#Example 2 Another way of creaitng dictioanry

myDict = {} # creates empty dict. then initializing one item by one item
myDict["one"] = 1 
myDict["two"] = 2 
myDict[3] = "three" 
myDict["four"] = 4.4# {one : 1, two :2 , 3:three....}
print(myDict[3])
for key, val in myDict.items(): # always remember, to traverse thru the dictioanry items, we use items() to go item by item in dictioanry. Especially used in for loop in dictioanry.
    print(key, val) # prints key and value without colons in between. leaves spaces like this. one 1
print(myDict) # prints in a dictioanry style with colons and quotations.   


#list1=[]
#list1.append('apple')
