#store the human preproinsulin sequence in a variable called preproinsulin
preproInsulin = "malwmrllpllallalwgpdpaaafvnqhlcgshlvealylvcgergffytpktr" \
"reaedlqvgqvelgggpgagslqplalegslqkrgiveqcctsicslyqlenycn"
 
 # backslash\ in variable value is used to maintain compliance with PEP 8 style guide
 # If there is a lengthy line, you can split variables and code into smaller blocks\
 #for better readability and simplicity
 #PEP- Python Enhancement Proposals - recommends a max of 79 characters per line
 
 #store the remaining sequence elements of human insulin in variables
 
isInsulin ="malwmrllpllallalwgpdpaaa"
bInsulin ="fvnqhlcgshlvealylvcgergffytpkt"
aInsulin ="giveqcctsicslyqlenycn"
cInsulin ="rreaedlqvgqvelgggpgagslqplalegslqkr"
insulin=bInsulin + aInsulin
print(insulin)
print(f'  The sequence of human preproInsulin: " {preproInsulin}')
print("The sequence of human insulin, chain a: " + aInsulin) # you can concatenate strings in the print function using + operator or by just adding the variable name after double quotes.
print("The sequence of human insulin, chain a:", aInsulin)

# Calculating the molecular weight of insulin  
# Creating a list of the amino acid (AA) weights  
aaWeights = {'A': 89.09, 'C': 121.16, 'D': 133.10, 'E': 147.13, 'F': 165.19,
'G': 75.07, 'H': 155.16, 'I': 131.17, 'K': 146.19, 'L': 131.17, 'M': 149.21,
'N': 132.12, 'P': 115.13, 'Q': 146.15, 'R': 174.20, 'S': 105.09, 'T': 119.12,
'V': 117.15, 'W': 204.23, 'Y': 181.19}  
# Count the number of each amino acids  
aaCountInsulin = ({x: float(insulin.upper().count(x)) for x in ['A', 'C',
'D', 'E', 'F', 'G', 'H', 'I', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T',
'V', 'W', 'Y']})  
# Multiply the count by the weights  
molecularWeightInsulin = sum({x: (aaCountInsulin[x]*aaWeights[x]) for x in
['A', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K', 'L', 'M', 'N', 'P', 'Q', 'R',
'S', 'T', 'V', 'W', 'Y']}.values())  
print("The rough molecular weight of insulin: " +
str(molecularWeightInsulin))
#print(aaCountInsulin)
#print(insulin.upper().count("F"))
molecularWeightInsulinActual = 5807.63
print("Error percentage: " + str(((molecularWeightInsulin - molecularWeightInsulinActual)/molecularWeightInsulinActual)*100))

#Ex-1:
string1 = "Apple"
print(string1.count('p'))

b = ({x: string1.lower().count(x) for x in ['a','p','e']})
#b={}
#for x in ['a','p','e']:
 #   b[x]= string1.lower().count(x)
#print(b)
print(sum(b.values()))


