#Create a program that tells us how many days, weeks, months we have left if we live until 90 years old.

age=int(input("Enter your current age in years: "))
years=90-age # subtract the age entered by the user
months=years*12 # multiply the number of years times 12 to get the number of months 
weeks=years*52
days=years*365
print(f'You have {years} years, {months} months, {weeks} weeks, {days} days left if you live until 90 years old')

# Division operator / by default returns float value. int("7")/int(3.5) =7/3.5=2 which is int. but it still returns float.
a=int("6")/int(2.0)
print(a)
print(type(a)) # returns float

# floor method

# math.floor(5.6) returns 5
# math.floor(0.6) returns 0
# math.floor(3.2) returns 3 if it is postive number, it ROUNDS DOWN
# math.floor(-46.8) returns 47
# math.floor(-8.7) returns 9 if it is a negative number, it ROUNDS UP to the next number
