#The tuple is like a list, but it can't be changed. 
#A data type that can't be changed after it's created is said to be immutable.
#To define a tuple, you use parentheses instead of brackets ([]).

#Define a tuple

mytuple = ("geetha","arun","janya","yuthi")
print(mytuple)
print(type(mytuple))
print(mytuple[0])
print(mytuple[1])
print(mytuple[2])
print(mytuple[3])

