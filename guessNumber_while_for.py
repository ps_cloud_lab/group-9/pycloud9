import random
random_number = random.randint(1,11)
guessing = False
while guessing == False:
	guess = input("Enter your guessed number: ")
	if(int(guess)==random_number):
		print(f' That"s a right guess! You guessed {guess}')
		guessing=True
	else:
		print("Sorry! That's a wrong guess. Try again!")
	
