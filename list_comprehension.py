#List Comprehension to extract the passed students - my own example to understand better

student_scores = {"Arun": "Pass", "Jayden":"Pass","Emily":"Fail","Krish":"Pass"}
# for x ,y in student_scores.items():
#     if y == "Pass":
#         print(x) # these 3 lines of code will be done in 1 line using list comprehension

passStuList=[x for x, y in student_scores.items() if y=="Pass"]  #using list format      
print(passStuList)

passStudent = {x : y for x, y in student_scores.items() if y=="Pass"} # using dict format. creates a dict and extracts passed students 
print(passStudent)

passStuList=[x for x in student_scores.keys()]   # creates a list since we gave list. gives only key values using keys()    
print(passStuList)

passStuList=[x for x in student_scores.values() if x=="Pass"]   # gives only the values from the dictionary     
print(passStuList)

#using format

x=[1,2,3,4]
#x=(sum(6,4))-(sum(4,3))
print(sum(x))
print('{0:.3f}'.format(5.5335))