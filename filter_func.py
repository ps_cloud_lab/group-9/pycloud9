# Using filter() built-in function
#The filter() function takes two arguments:
#function - a function
#iterable - an iterable like sets, lists, tuples etc.
#Note: You can easily convert iterators to sequences like lists, tuples, strings etc.
#what does the filter function return?
#The filter() function returns an iterator.
#The filter() function extracts elements from an iterable (list, tuple etc.) for which a function returns True
#The filter function takes in two arguments: a function that returns True or False (checks for a specific condition) and the iterable object we want to apply it to (such as a list in this case)
# returns the odd numbers for the numbers given

#prints even numbers
numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

# returns True if number is even
def check_even(x):
    if x % 2 == 0:
          return True  

    return False

# Extract numbers from the numbers list for which check_even() returns True
even_numbers_iterator = filter(check_even, numbers)

# converting to list
#print(even_numbers_iterator) # you cannot print the contents of this varable since it contains the object of filter function
even_numbers = list(even_numbers_iterator)
print(even_numbers)
print("----------------------------")
# or you can use for loop to iterate thru each number from the even_number, If you give both for loop and print() together,for loop wont work.either one will work since both does the same. 
#for x in even_numbers:
 #   print(x)


# Using lambda function to iterate even numbers
# its a shorthand for defining functions. To write concise code without wasting multiple lines defining a function
# Lambda function is used when we rquire a nameless function for shorter time period.

numbers = [1, 2, 3, 4, 5, 6, 7]

# the lambda function returns True for even numbers 
even_numbers_iterator = filter(lambda y: (y%2 == 0), numbers)

# converting to list
even_numbers = list(even_numbers_iterator)

print(even_numbers)
print("----------------------------------------")

#program to check odd numbers
#numbers=(1,2,3,4,5,6,7,8,9,10) # you can give the tuple like this or you can assigns numbers in a range like below  which is easier
numbers=range(1,20)
def odd_number(a): # defines the function odd_number
    if a%2 != 0: 
        return True
    else:
        return False

odd_number_list = filter(odd_number,numbers)
print(list(odd_number_list))
print("-------------------------------------")
#or you can use for loop to iterate thru each number from the odd_number_list, If you give both for loop and print() together,for loop wont work.either one will work since both does the same. 
#for a in odd_number_list: 
 #   print(a)
    
#printing vowels using filter function

string1=("g","e","e","t","h","a")
def vowel_func(x):
    vowel_letters=("a","e","i","o","u")
    if x in vowel_letters:
        return True
    else:
        return False
vowels=filter(vowel_func, string1)  
print(list(vowels))
print("---------------------------------------")
for x in vowels: # you can also use for loop to list the o/p
   print(x)

# Program to categorize adults
ages = [5,9,12,18,24,37]
def myFunc(x):
    if x<18:
        return False # says don't return the values
    else:
        return True # returns the values only for the true condition. here true will evaluate for ages > 18

adults=filter(myFunc, ages) 
#print(adults) - just giving adults will not print anything except the object number.we need to give within lists, since the o/p of myFunc is a list
print(list(adults)) 

for x in adults:
    print(x)
    
   
