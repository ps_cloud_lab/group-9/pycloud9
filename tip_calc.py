# calculate each person's share with the tip that each person has to pay in a restaurant

bill_amt=float(input("Enter the bill amount in dollars: "))
tip_percentage=float(input("Enter the percentage of tip you want to give. Don't put % sign: 12%, 15%, 18% :"))
num_of_persons=float(input("Enter the number of persons sharing this bill: "))
each_person_tip_share=(((bill_amt*tip_percentage/100)+bill_amt)/num_of_persons)
final_amt=round(each_person_tip_share, 2) # will round the amount to 2 deciaml places.
print(f'Each person"s share will be ${final_amt}')
