import re

txt1='''ORIGIN      
        1 malwmrllpl lallalwgpd paaafvnqhl cgshlvealy lvcgergffy tpktrreaed
       61 lqvgqvelgg gpgagslqpl alegslqkrg iveqcctsic slyqlenycn
//'''
y=re.sub("ORIGIN","",txt1)
#print(y)
y=re.sub("\d","",y)
#print(y)
y=re.sub("\s","",y)
#print(y)
y=re.sub("//","",y)
print(f' Cleaned insulin text file: {y}')
print(len(y))