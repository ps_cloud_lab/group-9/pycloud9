# using multiple break points, the program stops at first break point and then resumes to the 
#next break point when you click on blue play button and continues to other break points until the end of the program.
#step into option navigates to line by line debugging.
# Refer lab 16

name = "John"
print("Hello " + name + ".")
age = 40
print(name + " is " + str(age) + " years old.")
