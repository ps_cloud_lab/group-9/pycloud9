import csv # reads the csv file
import copy # makes a deep copy in memory

myVehicle = {
    "vin" : "<empty>",
    "make" : "<empty>" ,
    "model" : "<empty>" ,
    "year" : 0,
    "range" : 0,
    "topSpeed" : 0,
    "zeroSixty" : 0.0,
    "mileage" : 0
}

for key, value in myVehicle.items(): #item() belongs to dictionary data type. It tells for loop to traverse the collection in the dictioanry
    print("{} : {}".format(key,value)) # or print(f'{key},{value}')
myInventoryList = [] # empty list to hold the car inventory data

with open("car_fleet.csv") as csvFile:
    csvReader = csv.reader(csvFile)
    firstLine = next(csvReader) # reads the current line and transfers to the second line
    print(f'{",".join(firstLine)}') # puts comma in between the fields
    x=0
    for i in csvReader:
        print(i) # not needed. Just to see the display, we give this
        print(f' vin: {i[0]}, make: {i[1]}, model: {i[2]}, year: {i[3]}, range: {i[4]}, topspeed: {i[5]}, zeroSixty: {i[6]}, mileage: {i[7]}') # assigns the value to the key for each record
        currentVehicle = copy.deepcopy(myVehicle)
        currentVehicle["vin"] = i[0]
        currentVehicle["make"] = i[1]
        currentVehicle["model"] = i[2]
        currentVehicle["year"] = i[3]
        currentVehicle["range"] =i[4]
        currentVehicle["topspeed"] = i[5]
        currentVehicle["zeroSixty"] = i[6]
        currentVehicle["mileage"] = i[7]
        
        myInventoryList.append(currentVehicle)
        
        var1=myInventoryList[x]
        print(f'Last output: {var1}')
        x=x+1
        
        
    for n in myInventoryList:
        for key,value in n.items():
           print(f'{key}:{value}')
        print("-------")
    
        
        
        


