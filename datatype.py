myvalue=3.14
print(myvalue)
print(str(myvalue) + " is of the data type " + str(type(myvalue)))

# using imaginary number

myvalue=5j
print(myvalue)
print(str(myvalue) + " is of the data type " + str(type(myvalue)))

# using boolean data type
myvalue=True
print(myvalue)
print(str(myvalue) + " is of the data type " + str(type(myvalue)))