import io
with io.open("myclean.txt","r",encoding='utf-8') as f:
    text=f.read()
    print(text)
z=text[89:]
print(f' Amino acids 90-110: {z}, {len(z)}') # instead of giving one more print for len(z), we can give here within the same print statement.
with io.open("ainsulin-seq-clean.txt","w",encoding='utf-8') as f:
    f.write(z)
