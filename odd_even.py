#Write a program that works out whether if a given number is an odd or even number.
num=int(input("Enter a whole number: "))
if num % 2 == 0:
    print(f'You entered {num} which is an even number')
elif num % 2 != 0:
    print(f'You entered {num} which is an odd number')
else:
    print("Enter a valid number")
