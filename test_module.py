# from mymodule import even_print
# even_print(70)

import mymodule 
print(mymodule.even(81)) # since this function has a return value, we can print the value or store it in a variable. any variable which has a value in it can be printed
mymodule.even_print(42) # since no return in the function, just call the function and no print is used.


