myString = "This is a string"
print(myString)
print(type(myString))
print(myString + " is of the data type " + str(type(myString)))

# concatenate two strings

first = "water"
second = "fall"
third = first + second
print(third)

# Input Strings using input function

name = input("What is your name? ") # If you give space between ? and "", you will have a space after question
print(name)

# Get the input from the user and output it back to the user

color = input("what is your favorite color? ")
animal = input("what animal do you like? ")
print("{}, you like a {} {}!".format(name,color,animal)) # curly braces acts as the placeholder for the variables that is passed to the format function. check the o/p.