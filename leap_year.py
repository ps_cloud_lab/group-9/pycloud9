#Write a program that works out whether if a given year is a leap year.
#A normal year has 365 days, leap years have 366, with an extra day in February.
year=int(input("Enter the year you want to check if it is a leap year or not: "))
if year % 4 == 0: # First check,if the year evenly divisible by 4.  If yes, then leap year.If you get remainder then it is not leap year. but still check all 3 conditons
    if year % 100 == 0:  # secondly, check if the year is evenly divisible by 100. If yes=definitely leap year.If not, go to the next step to check if it satisfies the next condition
        if year % 400 == 0:  # lastly, check if the year is evenly divisible by 400. If yes, it is leap year.
            print("Leap year")
        else:
            print("Not a leap year")   
    else:
        print("Leap year")
else:
    print("Not a leap year")    
    

#ex1
#2040/4=510(leap)
#2040/100=20.4 (Leap)
#2040/400=5.1(not leap)
#since first 2 conditons satisfied, it is LEAP year

#ex2
#2100/4=525(leap)
#2100/100=21(not leap)
#2100/400=5.25(not leap)
#since last 2 conditons are not met, it is NOT a leap year

#ex3
#2000/4=500(leap)
#2000/100=20(not leap)
#2000/400=5(leap)
#since first and third conditons are met,it is a LEAP year.
#first condition should DEFINITELY be met in order for the year to be leap year.
